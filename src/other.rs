use std::path::PathBuf;

use plotters::style::RED;

mod plot;

mod math {
    pub(super) fn inverse(n: u128) -> Option<f64> {
        match n {
            0 => None,
            _ => Some(1.0 / (n as f64)),
        }
    }

    pub(super) fn pow(base: f64, exponent: usize) -> f64 {
        match exponent {
            0 => 1.0,
            1 => base,
            _ => base * pow(base, exponent - 1),
        }
    }
}

mod proba {
    use super::math::{inverse, pow};

    pub(super) fn upper_bound(q: u128, n: usize, m: usize) -> f64 {
        let p = n.max(m);
        let q_inv = inverse(q).unwrap();

        pow(1.0 - q_inv, p)
    }

    pub(super) fn exact(q: u128, n: usize, m: usize) -> f64 {
        let mut res = 1.0;
        let (n, m) = (n.max(m), n.min(m));

        let q_inv = inverse(q).unwrap();

        for i in (n - m + 1)..n {
            res *= 1.0 - pow(q_inv, i);
        }

        res
    }
}

const FIGURE_PATH: &str = "scatterplot.png";

fn main() {
    let n = 10;
    let m = 20;

    let xs = [10, 100, 1_000, 10_000, 100_000];

    let x_max = *xs.iter().max().unwrap_or(&0) as f64;

    let ys = xs
        .iter()
        .map(|&x| proba::exact(x, n, m))
        .collect::<Vec<_>>();

    let xs = xs.iter().map(|&x| x as f64).collect::<Vec<_>>();

    plot::plot(
        &xs,
        &ys,
        &format!("m = {}", m),
        &RED,
        &PathBuf::from(FIGURE_PATH),
        0f64..x_max,
        0f64..1f64,
    )
    .unwrap();
}
