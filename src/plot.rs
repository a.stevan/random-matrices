use std::{ops::Range, path::PathBuf};

use plotters::prelude::*;

pub(crate) fn plot(
    xs: &[f64],
    ys: &[f64],
    label: &str,
    color: &RGBColor,
    figure_path: &PathBuf,
    x_bounds: Range<f64>,
    y_bounds: Range<f64>,
) -> Result<(), Box<dyn std::error::Error>>
{
    let root = BitMapBackend::new(figure_path, (1920, 1080)).into_drawing_area();
    root.fill(&WHITE)?;

    let mut chart = ChartBuilder::on(&root)
        .caption("Scatter Plot", ("Arial", 20))
        .margin(20)
        .x_label_area_size(10)
        .y_label_area_size(10)
        .build_cartesian_2d(x_bounds, y_bounds)?;

    chart.configure_mesh().draw()?;

    chart
        .draw_series(
            xs.iter()
                .zip(ys.iter())
                .map(|(&x, &y)| Circle::new((x as f64, y), 5, Into::<ShapeStyle>::into(&color))),
        )?
        .label(label)
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x - 20, y)], *color));

    chart
        .configure_series_labels()
        .position(SeriesLabelPosition::LowerRight)
        .margin(20)
        .legend_area_size(5)
        .border_style(BLUE)
        .background_style(BLUE.mix(0.1))
        .label_font(("Calibri", 20))
        .draw()?;

    Ok(())
}
