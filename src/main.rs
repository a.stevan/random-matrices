use std::path::PathBuf;

use plotters::style::RED;

mod plot;

/// compute $\Pi_{i=m+1}^{\infty}(1 - \frac{1}{q^j})$
fn proba(m: u32, q: u128, precision: f64) -> f64 {
    let mut p = 1.0;
    let mut prev = f64::MAX;

    let mut i = m + 1;

    // compute the first inverse power of $q$, $\frac{1}{q^{m + 1}}$
    let q_inv = 1.0 / q as f64;
    let mut q_inv_pow = (0..i).fold(1.0, |acc, _| acc * q_inv);

    while (p - prev).abs() > precision {
        prev = p;
        p *= 1.0 - q_inv_pow;

        q_inv_pow *= q_inv;
        i += 1;
    }

    p
}

const PRECISION: f64 = 1e-10;
const FIGURE_PATH: &str = "scatterplot.png";

fn main() {
    let xs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15];
    let m = 2;

    let x_max = *xs.iter().max().unwrap_or(&0) as f64;

    let ys = xs
        .iter()
        .map(|&x| proba(m, x, PRECISION))
        .collect::<Vec<_>>();
    let xs = xs.iter().map(|&x| x as f64).collect::<Vec<_>>();

    plot::plot(
        &xs,
        &ys,
        &format!("m = {}", m),
        &RED,
        &PathBuf::from(FIGURE_PATH),
        0f64..x_max,
        0f64..1f64,
    )
    .unwrap();
}
